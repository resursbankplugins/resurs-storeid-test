# Resurs Bank StoreID Testing Plugin (Wordpress) #

### What is this repository for? ###

Testing Resurs Bank feature storeId in the shopwflows

### How do I get set up? ###

* Put the php-file in your plugin directory of Wordpress.
* Enable the plugin
* Setting storeId to an empty value will "disable" the payload inject

### Who do I talk to? ###

This plugin is written for internal tests only, but is availabe in this community for others too. The developer can be contacted via tomas.tornevall@resurs.se
