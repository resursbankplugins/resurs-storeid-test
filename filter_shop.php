<?php
/*
Plugin Name: Addon test plugin for resurs-bank-payment-gateway-for-woocommerce
Plugin URI:
WC Tested up to: 3.2.3
Description: Test plugin for pushing storeId into Resurs Bank payment payload
Author: Tomas Tornevall
Version: 1.0.1
Author URI: https://test.resurs.com/docs/x/BQBu
*/

if (!session_id()) {
    session_start();
}
function setResursStoreIdForm($items)
{
    $storeId = setResursStoreId();
    $items .= '
		<li style="font-size:10px"> 
		<form>
			Store <input style="border:1px dashed black; font-size:10px;" id="resursStoreId" value="' . $storeId . '">
			<input type="button" style="font-size: 10px;" value="Save" onclick="memorizeResursStoreId()">
		</form>
		</li>
		';

    return $items;
}

function resursStoreIdMemoryScript()
{
    echo '
		<script>
		    function memorizeResursStoreId() {
	            jQuery.ajax(
	                {
	                    "url": "?memorizeResursStoreId=" + jQuery("#resursStoreId").val()
	                }
	            ).done(function (x, y, z) {
	                if (typeof x["successStoreId"] !== "undefined") {
	                	alert(x["successStoreId"]);
	                }
	            });
        	}
		</script>
	';
}

function setCheckoutDisabled()
{
    if (isset($_COOKIE['resursStoreId']) && $_COOKIE['resursStoreId'] == "12") {
        return true;
    }
}

function setResursStoreId()
{
    if (isset($_COOKIE) && isset($_COOKIE['resursStoreId'])) {
        return $_COOKIE['resursStoreId'];
    }
    return "";
}

if (isset($_REQUEST['memorizeResursStoreId'])) {
    if (!session_id()) {
        session_start();
    }
    header("Content-Type: application/json");
    $successStoreId = false;
    if (isset($_REQUEST['memorizeResursStoreId'])) {
        setcookie("resursStoreId", $_REQUEST['memorizeResursStoreId'], (time() + 86400 * 7), "/");
        $successStoreId = true;
    }
    echo json_encode(["successStoreId" => $successStoreId]);
    die();
}

add_action('wp_head', 'resursStoreIdMemoryScript');
add_filter('resursbank_set_storeid', 'setResursStoreId');
add_filter('resursbank_temporary_disable_checkout', 'setCheckoutDisabled');
add_filter('wp_nav_menu_items', 'setResursStoreIdForm', 999);
